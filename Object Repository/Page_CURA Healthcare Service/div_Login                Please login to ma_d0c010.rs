<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Login                Please login to ma_d0c010</name>
   <tag></tag>
   <elementGuidId>61ec9da0-5eec-499e-9e0d-ada36f5f58fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-sm-12.text-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='login']/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d597c8ea-aaa5-4649-b97f-d2c056c6c5f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-sm-12 text-center</value>
      <webElementGuid>12ce0e39-3e9a-4744-8b06-95516600d362</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Login
                Please login to make appointment.
                                    Login failed! Please ensure the username and password are valid.
                            </value>
      <webElementGuid>b4562c44-3a06-40e1-9685-8285f1add168</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-12 text-center&quot;]</value>
      <webElementGuid>967271b6-856f-4df3-9802-7de46797b75f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='login']/div/div/div</value>
      <webElementGuid>1505e37c-b5ee-467b-ad83-17eb3a09536f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::div[3]</value>
      <webElementGuid>23129f75-ba95-4b6f-b4dc-f83c7de9b77e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='We Care About Your Health'])[1]/following::div[3]</value>
      <webElementGuid>d0cb1354-39e7-4df2-8db6-a5bc577bf168</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Demo account'])[1]/preceding::div[1]</value>
      <webElementGuid>e817cfcb-22f7-497b-bc38-1956e6369785</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div</value>
      <webElementGuid>a2a1f96d-be7a-4750-a0d4-ac0f0c898555</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                Login
                Please login to make appointment.
                                    Login failed! Please ensure the username and password are valid.
                            ' or . = '
                Login
                Please login to make appointment.
                                    Login failed! Please ensure the username and password are valid.
                            ')]</value>
      <webElementGuid>f4346b45-6801-4feb-a273-14c2c7bf493a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
